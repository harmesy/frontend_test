# Frontend Test

## Instructions

Create a webpage based on the provided photoshop mock. The webpage should use a 1440-grid system and be responsive. Use your best judgement in adapting the layout to smaller screen sizes.

In addition to converting the provided photoshop mockup into a webpage, here are some further instructions:

* Clicking the `down` arrow should smoothly scroll the page to the `team` section.
* The dropdown should have the following options: (1) All, (2) Heroes, (3) Villains.
* Selecting Heroes from the dropdown should filter the list to show only heroes. The state should be encapsulated in the URL.
* Selecting Villains from the dropdown should filter the list to show only villains. The state should be encapsulated in the URL.
* The form should submit to the FormKeep web application. Details are below.

You will need to create a single page. Use any templating language and CSS/JS libraries/frameworks that you feel will help you accomplish this task as efficiently as possible. If you use any precompilers/build tools, please include the source files so that we can evaluate your work. Also, include the built files so that we can view your work without needing to setup any tools on our workstation.

### The Form

The form will be submitted to the FormKeep web application. You do not need
access to this application.

Submit data via a POST request to [https://formkeep.com/f/3470cf16d02a](https://formkeep.com/f/3470cf16d02a).

More information on using FormKeep is available here: [https://formkeep.com/help](https://formkeep.com/help).

### The Photoshop Template

The Photoshop template you will be working from has been included in this repository. You will find it at [assets/mockup.psd.zip](assets/mockup.psd.zip).

### Submission

Place your work in the solution folder and submit a pull request. Please do not check in node modules, composer libraries, ruby gems, etc.
