require 'bundler'

Bundler.require(:default)

set :bind, '0.0.0.0'
set :port, '3000'
set :public_folder, File.dirname(__FILE__) + '/assets'

get '/' do
  file = File.read('team_members.json')
  team_members = JSON.parse(file)

  erb :index, locals: { team_members: team_members }
end
