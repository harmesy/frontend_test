$(document).ready(function() {
  if (window.location.hash) {
    filter(window.location.hash.slice(1));
  }

  $(window).on('hashchange', function() {
    if (window.location.hash) {
      filter(window.location.hash.slice(1));
    }
  });

  $('.dropdown-menu a').click(function() {
    $('#teamDropdown').dropdown('toggle');

    var filter_value = $(this).data('filter');

    filter(filter_value);
  });

  $('#down_btn').click(function() {
    $('html, body').animate({
      scrollTop: $("#teamListing").offset().top
    }, 1000);

    return false;
  });
});


function filter(filter_value) {
  if (filter_value == 'all') {
    $("#dropdownSelected").html("All Images");
    $('.member').fadeIn();
  } else if (filter_value == 'heroes') {
    $("#dropdownSelected").html("Heroes");
    $('.member').filter('.hero').fadeIn();
    $('.member').filter('.villain').fadeOut();
  } else if(filter_value == 'villains') {
    $("#dropdownSelected").html("Villains");
    $('.member').filter('.villain').fadeIn();
    $('.member').filter('.hero').fadeOut();
  }
}
